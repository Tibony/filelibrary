// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#include "FileLibraryBPLibrary.h"
#include "FileLibrary.h"

UFileLibraryBPLibrary::UFileLibraryBPLibrary(const FObjectInitializer& ObjectInitializer)
: Super(ObjectInitializer)
{

}

void UFileLibraryBPLibrary::GetAllPaths(TArray<FString>& Files, const FString & FilePath, const FString& Extension)
{
	FString SearchedFiles = FilePath + Extension;
	TArray<FString> FindedFiles;
	IFileManager::Get().FindFiles(FindedFiles, *SearchedFiles, true, false);
	FString SearchFile = "";
	for (int i = 0; i < FindedFiles.Num(); i++)
	{
		SearchFile = FilePath + FindedFiles[i];
		Files.Add(SearchFile);
	}
}

void UFileLibraryBPLibrary::CleanFile(FString Path)
{
	IPlatformFile& PlatfromFile = FPlatformFileManager::Get().GetPlatformFile();
	PlatfromFile.DeleteFile(*Path);

}

FString UFileLibraryBPLibrary::GetStringFromFile(FString Path)
{
	FString LocalString;
	FFileHelper::LoadFileToString(LocalString, *Path);

	return LocalString;
}

